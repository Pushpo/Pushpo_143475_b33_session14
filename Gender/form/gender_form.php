<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add gender</h2>
    <form>
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="email" class="form-control" id="name" placeholder="Enter Person's Name">
        </div>
        <div class="checkbox">
            <label for="name">Gender:</label>
            <label><input type="radio">Male</label>
            <label><input type="radio">Female</label>
        </div>
        <button type="submit" class="btn-danger">Submit</button>
    </form>
</div>

</body>
</html>