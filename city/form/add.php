<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Your City</h2>
    <form>
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="name" class="form-control" id="name" placeholder="Enter Person's name">
        </div>
        <div class="form-group">
            <label for="city">City:</label>
            <select class="btn-lg">
                <option value="Chittagong">Chittagong</option>
                <option value="Dhaka">Dhaka</option>
                <option value="Khulna">Khulna</option>
                <option value="Barsial">Barsial</option>
                <option value="Sylhet">Barsial</option>
            </select>
        </div>

        <button type="submit" class="btn-danger">Submit</button>
    </form>
</div>

</body>
</html>