<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add Book Title Name</h2>
    <form>
        <div class="form-group">
            <label for="booktitle">Book Title:</label>
            <input type="booktitle" class="form-control" id="booktitle" placeholder="Enter book title name ">
        </div>
        <div class="form-group">
            <label for="authorname">Author name:</label>
            <input type="authorname" class="form-control" id="authorname" placeholder="Enter author name">
        </div>

        <button type="Add" class="btn-info">Add</button>
        <button type="Edit" class="btn-info">Edit</button>
    </form>
</div>

</body>
</html>