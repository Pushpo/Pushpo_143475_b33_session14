<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/js/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add your Hobbies</h2>
    <form>
        <div class="form-group">
            <label for="Name">Name:</label>
            <input type="Name" class="form-control" id="Name" placeholder="Enter Name">
        </div>
        <div class="form-group">
            <label for="hobbies">Hobbies:</label>
        </div>
        <div class="checkbox">
            <label><input type="checkbox">Gardening</label>
            <label><input type="checkbox"> Reading Story Books</label>
            <label><input type="checkbox"> Cricket</label>
            <label><input type="checkbox"> Music</label>
            <label><input type="checkbox"> Football</label>
            <label><input type="checkbox"> others</label>
        </div>
        <button type="submit" class="btn-danger">Submit</button>
    </form>
</div>

</body>
</html>